{{-- Layout del que estamos extendiendo --}}
@extends('layouts.main')
{{-- Sección que sera colocada en el layut main --}}
@section('dashboard')

{{-- Nombre del subtitulo de bajo del nombre Dashboard --}}
@section('Subtitle', '')
{{-- Se incluye el componente panel de control en esta vista --}}
@include('component.panel')
{{-- Tabla de datos --}}
{{-- Se recorre la variable solicitudes que es enviada desde el controlador --}}
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header text-center bg-primary text-light"><h5>{{ __('Registro de usuarios') }}</h5></div>

            <div class="card-body">
                <form method="POST" action="{{ route('user.store') }}">
                    @csrf

                    <div class="form-group row m-1">
                        <div class="col-md-12">
                            <div class="input-group">
                                <div class="input-group-text"> <span data-feather="user"></span>
                                </div>
                                <input id="name"  type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Nombre">
                            </div>

                            @if ($errors->has('name'))
                            
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row m-1">

                        <div class="col-md-12">
                            <div class="input-group">
                                <div class="input-group-text  "> <span   data-feather="mail"></span>
                                </div>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">
                            </div>


                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row m-1">

                        <div class="col-md-12">
                            <div class="input-group">
                                <div class="input-group-text"> <span data-feather="lock"></span>
                                </div>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Contraseña">
                            </div>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row m-1">

                        <div class="col-md-12">
                            <div class="input-group">
                                <div class="input-group-text"> <span data-feather="repeat"></span>
                                </div>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirma contraseña">
                            </div>
                        </div>
                    </div>
                    <div class="form-grup row m-1">
                        <div class="col-md-12">
                           
                            <div class="input-group">
                                <div class="input-group-text"> <span data-feather="briefcase"></span>
                                </div>
                                <select class="form-select" name="role" aria-label="Default select example">
                                    @foreach($all_roles_in_database as $item)
    
                                        <option class="" value="{{ $item }}">{{ $item }}</option>                               
                                            
                                    @endforeach
                                </select>  
                            </div>                          
                    </div>
                   

                    <div class="form-group row  mb-0 m-4">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-success ">
                                <span data-feather="save"></span>
                                {{ __('Guardar') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


</main>
</div>
</div>


@stop
    {{-- Termina la sección --}}
{{-- Layout del que estamos extendiendo --}}
@extends('layouts.main')
{{-- Sección que sera colocada en el layut main --}}
@section('dashboard')

{{-- Nombre del subtitulo de bajo del nombre Dashboard --}}
@section('Subtitle', '')
{{-- Se incluye el componente panel de control en esta vista --}}
@include('component.panel')
{{-- Tabla de datos --}}
{{-- Se recorre la variable solicitudes que es enviada desde el controlador --}}


@include('component.message')


{{-- Inicio del formulario --}}


<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header text-center bg-primary text-light"><h5>{{ __('Registro de roles') }}</h5></div>

            <div class="card-body">
                <form method="POST" action="{{ route('role.store') }}">
                    @csrf
                                    

                    <div class="form-group row m-1">

                        <div class="col-md-12">
                            <div class="input-group">
                                <div class="input-group-text"> <span data-feather="briefcase"></span>
                                </div>
                                <input id="role" type="text" class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role" required placeholder="Nombre del role">
                            </div>

                            @if ($errors->has('permiso'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('permiso') }}</strong>
                                </span>
                            @endif
                        </div>
               
                        <div class="card m-1">
                            <div class="card-header">
                                <h6 class="card-title">Permisos</h6>
                            </div>                            
                            <div class="card-body">
                                                             
                              @foreach($all_roles_in_database as $item)
                
                
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="permisos[]" value="{{ $item }}" type="checkbox" id="{{ $item }}">
                                    <label class="form-check-label text-uppercase" for="{{ $item }}">{{ $item }}</label>
                                </div>
                
                            @endforeach
                            </div>
                          </div>
                    </div>             
                   

                    <div class="form-group row  mb-0 m-4">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-success ">
                                <span data-feather="save"></span>
                                {{ __('Guardar') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</main>
</div>
</div>


@stop
    {{-- Termina la sección --}}
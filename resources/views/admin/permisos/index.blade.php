{{-- Layout del que estamos extendiendo --}}
@extends('layouts.main')
{{-- Sección que sera colocada en el layut main --}}
@section('dashboard')

{{-- Nombre del subtitulo de bajo del nombre Dashboard --}}

@section('Subtitle', 'Nuevas solicitudes')

{{-- Se incluye el componente panel de control en esta vista --}}
@include('component.panel')

@include('component.message')
{{-- Tabla de datos --}}
{{-- Se recorre la variable solicitudes que es enviada desde el controlador --}}

<div class="btn-toolbar mb-2 mb-md-0">
    <div class="btn-group m-2">
        <a type="button" class="btn btn-sm btn-success" href="{{route('permiso.create')}}">
            <span data-feather="key"></span>
            Nuevo permiso
        </a>    
      <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
    </div>
    {{--  <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
      <span data-feather="calendar"></span>
      This week
    </button>  --}}
  </div>
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>               
                <th scope="col">Fecha</th>
                <th scope="col">Opciones</th>
            </tr>
        </thead>
        <tbody>
            {{-- Se recorre la variable solicitudes que es enviada desde el controlador --}}
            @foreach($permisos as $permiso)

                    <tr>
                        <td scope="row">{{ $permiso->id }}</td>
                        <td>{{ $permiso->name }}</td>
                        <td>{{ $permiso->created_at->format('d-m-Y') }}</td>

                   
                        <td >
                            <div class="btn-group">
                                {{--  <a href="#" class="btn btn-primary">
                                    <span data-feather="eye"></span>
                                </a>  --}}
                                <a href="{{route('permiso.destroy',Crypt::encrypt($permiso->id))}}" class="btn btn-danger">
                                    <span data-feather="trash-2"></span>
                                </a>
                                
                              </div>                         
                        </td>
                    </tr>
            @endforeach

        </tbody>
    </table>
</div>

</main>
</div>
</div>


@stop
    {{-- Termina la sección --}}
{{-- Layout del que estamos extendiendo --}}
@extends('layouts.main')
{{-- Sección que sera colocada en el layut main --}}
@section('dashboard')

{{-- Nombre del subtitulo de bajo del nombre Dashboard --}}
@section('Subtitle', '')
{{-- Se incluye el componente panel de control en esta vista --}}
@include('component.panel')
{{-- Tabla de datos --}}
{{-- Se recorre la variable solicitudes que es enviada desde el controlador --}}


@if(session()->has('msj'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session('msj') }}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif

@if(session()->has('msjError'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>{{ session('msjError') }}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif


{{-- Inicio del formulario --}}


<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header text-center bg-primary text-light"><h5>{{ __('Registro de permisos') }}</h5></div>

            <div class="card-body">
                <form method="POST" action="{{ route('permiso.store') }}">
                    @csrf
                                    

                    <div class="form-group row m-1">

                        <div class="col-md-12">
                            <div class="input-group">
                                <div class="input-group-text"> <span data-feather="key"></span>
                                </div>
                                <input id="permiso" type="text" class="form-control{{ $errors->has('permiso') ? ' is-invalid' : '' }}" name="permiso" required placeholder="Nombre del permiso">
                            </div>

                            @if ($errors->has('permiso'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('permiso') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>             
                   

                    <div class="form-group row  mb-0 m-4">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-success ">
                                <span data-feather="save"></span>
                                {{ __('Guardar') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


</main>
</div>
</div>


@stop
    {{-- Termina la sección --}}
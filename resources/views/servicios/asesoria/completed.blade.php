<div class="table-responsive">
    <table class="table table-striped table-sm">
        <thead>
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Teléfono</th>
                <th>Email</th>
                <th>Opciones</th>
            </tr>
        </thead>
        <tbody>
            {{-- Se recorre la variable solicitudes que es enviada desde el controlador --}}
            @foreach($solicitudes as $solicitud)
                @if($solicitud->atendido == 1)

                    <tr>
                        <td>{{ $solicitud->id }}</td>
                        <td>{{ $solicitud->nombre }}</td>
                        <td>{{ $solicitud->telefono }}</td>
                        <td>{{ $solicitud->email }}</td>
                        <td>
                            <div class="btn-group">
                               
                                <a href="{{route('asesoria.show',Crypt::encrypt($solicitud->id))}}" class="btn btn-success">
                                    <span data-feather="eye"></span>
                                </a>                             
                                                                
                              </div>  
                        </td>
                    </tr>
                @endif
            @endforeach

        </tbody>
    </table>
</div>
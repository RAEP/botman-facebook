{{-- Layout del que estamos extendiendo --}}
@extends('layouts.main')
{{-- Sección que sera colocada en el layut main --}}
@section('dashboard')

{{-- Nombre del subtitulo de bajo del nombre Dashboard --}}
@section('Subtitle', 'Nuevas solicitudes')
{{-- Se incluye el componente panel de control en esta vista --}}
@include('component.panel')

@include('component.message')
{{-- Tabla de datos --}}
{{-- Se recorre la variable solicitudes que es enviada desde el controlador --}}
@forelse($solicitudes as $solicitud)



    @if($solicitud->atendido == 0)


        @switch($solicitud->instancia)
            @case(1)
            @role('PDT|super-admin')

                <div class="position-relative">
                    <div class="card m-3 col-8 top-0 start-50 translate-middle-x ">
                        <div class="card-header bg-primary text-white">
                            <strong>Folio {{ $solicitud->id }}</strong>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{ $solicitud->nombre }} {{ $solicitud->pApellido }}
                                {{ $solicitud->sApellido }}</h5>
                            <p class="card-text">{{ $solicitud->descripcion }}</p>

                            <a href="{{ route('asesoria.show',Crypt::encrypt($solicitud->id)) }}"
                                class="btn btn-warning">
                                <span data-feather="alert-triangle"></span> Atender
                            </a>

                        </div>
                        <div class="card-footer text-muted text-center ">
                            <span data-feather="calendar"></span>
                            <strong> {{ $solicitud->created_at->format('d-m-Y') }}</strong>
                        </div>
                    </div>
                </div>
            @endcan

            @break
            @case(2)
            @role('JI|super-admin')

                <div class="position-relative">
                    <div class="card m-3 col-8 top-0 start-50 translate-middle-x ">
                        <div class="card-header bg-primary text-white">
                            <strong>Folio {{ $solicitud->id }}</strong>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{ $solicitud->nombre }} {{ $solicitud->pApellido }}
                                {{ $solicitud->sApellido }}</h5>
                            <p class="card-text">{{ $solicitud->descripcion }}</p>

                            <a href="{{ route('asesoria.show', Crypt::encrypt($solicitud->id)) }}"
                                class="btn btn-warning">
                                <span data-feather="alert-triangle"></span> Atender
                            </a>

                        </div>
                        <div class="card-footer text-muted text-center ">
                            <span data-feather="calendar"></span>
                            <strong> {{ $solicitud->created_at->format('d-m-Y') }}</strong>
                        </div>
                    </div>
                </div>
            @endcan

            @break

            @case(3)
            @role('SEECAT|super-admin')

                <div class="position-relative">
                    <div class="card m-3 col-8 top-0 start-50 translate-middle-x ">
                        <div class="card-header bg-primary text-white">
                            <strong>Folio {{ $solicitud->id }}</strong>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{ $solicitud->nombre }} {{ $solicitud->pApellido }}
                                {{ $solicitud->sApellido }}</h5>
                            <p class="card-text">{{ $solicitud->descripcion }}</p>

                            <a href="{{ route('asesoria.show', Crypt::encrypt($solicitud->id)) }}"
                                class="btn btn-warning">
                                <span data-feather="alert-triangle"></span> Atender
                            </a>

                        </div>
                        <div class="card-footer text-muted text-center ">
                            <span data-feather="calendar"></span>
                            <strong> {{ $solicitud->created_at->format('d-m-Y') }}</strong>
                        </div>
                    </div>
                </div>
            @endcan

            @break
            @default
            <h1>hola</h1>
        @endswitch

    @else

        @include('servicios.asesoria.completed')

    @endif


@empty
    <p>No users</p>


@endforelse
{{--  
<div class="table-responsive">
    <table class="table table-striped table-sm">
        <thead>
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Primer Apellido</th>
                <th>Segundo Apellido</th>
                <th>Email</th>
                <th>Opciones</th>
            </tr>
        </thead>
        <tbody>
            {{-- Se recorre la variable solicitudes que es enviada desde el controlador 
@foreach($solicitudes as $solicitud)

                <tr>
                    <td>{{ $solicitud->id }}</td>
<td>{{ $solicitud->nombre }}</td>
<td>{{ $solicitud->pApellido }}</td>
<td>{{ $solicitud->sApellido }}</td>
<td>{{ $solicitud->email }}</td>
<td>
    <a href="{{ route('asesoria.show', $solicitud->id) }}" class="btn btn-success">Atender</a>
</td>
</tr>
@endforeach

</tbody>
</table>
</div>
--}}
</main>
</div>
</div>


@stop
    {{-- Termina la sección --}}
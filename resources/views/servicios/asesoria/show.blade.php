{{-- Layout del que estamos extendiendo --}}
@extends('layouts.main')
{{-- Sección que sera colocada en el layut main --}}
@section('dashboard')

{{-- Nombre del subtitulo de bajo del nombre Dashboard --}}
@section('Subtitle', 'Solicitud')
{{-- Se incluye el componente panel de control en esta vista --}}
@include('component.panel')

@include('component.message')
{{-- Tabla de datos --}}
<div class="card">
    <div class="card-header bg-success text-light">
        <strong>Folio {{ $solicitud->id }}</strong>
    </div>
    <div class="card-body">
        <h5 class="card-title">{{ $solicitud->nombre }} {{ $solicitud->pApellido }} {{ $solicitud->sApellido }}
        </h5>
        <p class="card-text text-justify">{{ $solicitud->descripcion }}</p>
        <p class="card-text"><strong>Teléfono:</strong> {{ $solicitud->telefono }}</p>

        <p class="text-justify"><strong>Email:</strong> {{ $solicitud->email }} </p>

        @if ($solicitud->atendido == 0)
        <form action="{{ route('asesoria.respuesta') }}" method="post">
            @csrf
            <div class="col-auto">
                
                <div class="visually-hidden">
                    <input type="text" name="id" id="" value={{ $solicitud->id }}>
                </div>
                <div class="visually-hidden">
                    <input type="text" name="nombre" id="" value={{ $solicitud->nombre }}>
                </div>
                <div class="input-group">
                    <div class="input-group-text"> <span data-feather="type"></span>
                    </div>
                    <textarea class="form-control" name="respuesta" id="" rows="3"
                        placeholder="Respuesta..."></textarea>
                </div>
            </div>
            <div class="col-auto mt-3">
                <button class="btn btn-primary" type="submit">
                    <span data-feather="send"></span>
                    Enviar
                </button>
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    <span data-feather="refresh-ccw"></span>
                    No me compete
                </button>
            </div>

        </form>

            
        @else
        <p class="text-justify"><strong>Respuesta:</strong> {{ $solicitud->respuesta }} </p>

            
        @endif
       
    </div>
    <div class="card-footer text-muted text-center ">
        <span data-feather="calendar"></span>
        <strong> {{ $solicitud->created_at->format('d-m-Y') }}</strong>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Instancia Responsable</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('asesoria.instancia') }}" method="post">
                    @csrf

                    <div class="visually-hidden">
                        <input type="text" name="id" id="" value={{ $solicitud->id }}>
                    </div>
                    <div class="list-group">
                        <label class="list-group-item">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" value="1" name="instancia"
                                    id="Procuraduria">
                                <label class="form-check-label" for="Procuraduria">
                                    Procuraduría de la Defensa del Trabajo
                                </label>
                            </div>
                        </label>
                        <label class="list-group-item">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" value="2" name="instancia"
                                    id="Jefatura" checked>
                                <label class="form-check-label" for="Jefatura">
                                    Jefatura de Inspecciones
                                </label>
                            </div>
                        </label>
                        <label class="list-group-item">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" value="3" name="instancia"
                                    id="seecat">
                                <label class="form-check-label" for="seecat">
                                    Servicio Estatal del Empleo y Capacitación para el Trabajo </label>
                            </div>
                        </label>

                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                    <span data-feather="x-circle"></span>
                    Cerrar</button>
                <button type="submit" class="btn btn-primary">
                    <span data-feather="send"></span>
                    Guardar cambios</button>
            </div>
            </form>
        </div>
    </div>
</div>

</main>
</div>
</div>


@stop
    {{-- Termina la sección --}}
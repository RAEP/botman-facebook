@extends('layouts.main')

@section('ALcreate')

    <!-- Content here -->

    
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Solicitud de Asesoría Laboral En Línea</h5>
            <p class="card-text text-justify">Si tienes dudas o aclaraciones y deseas preguntar directamente sobre
                sueldos y salarios,
                temas de inspecciones para la revisión de derechos y riesgos laborales, entre otros, con motivo de
                esclarecer tu duda,
                por favor llenar el siguiente formulario para que personal de la Procuraduría de la Defensa del Trabajo
                o de la Dirección
                del Trabajo (inspecciones) te contesten. Te enviaremos una respuesta al correo electrónico que nos
                proporciones, motivo por
                el cual te pedimos revises tu bandeja de correos no deseados en caso de no recibir respuesta.
                Cuéntenos sobre su duda laboral.</p>

        </div>
       
    </div>
    @if(session()->has('msj'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session('msj') }}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
     @endif

    @if(session()->has('msjError'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>{{ session('msjError') }}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
    @endif
   

    {{-- Inicio del formulario --}}
    <form class="position-relative" method="POST" action="{{route('createSoLa')}}">
       
        @csrf
        {{-- Contenedor para inputs del formulario --}}
        <div class="col-6 position-absolute top-0 start-50 translate-middle-x">
            <div class="col-auto m-3">
                <label class="visually-hidden" for="nombre">Nombre</label>
                <div class="input-group">
                    <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre">
                </div>
            </div>

            <div class="col-auto m-3">
                <label class="visually-hidden" for="pApellido">Primer Apellido</label>
                <div class="input-group">
                    <input type="text" name="pApellido" class="form-control" id="pApellido"
                        placeholder="Primer Apellido">
                </div>
            </div>
            <div class="col-auto m-3">
                <label class="visually-hidden" for="sApellido">Segundo Apellido</label>
                <div class="input-group">
                    <input type="text" name="sApellido" class="form-control" id="sApellido"
                        placeholder="Segundo Apellido">
                </div>
            </div>
            <div class="col-auto m-3">
                <label class="visually-hidden" for="numExterior">Username</label>
                <div class="input-group">
                    <div class="input-group-text"> <span data-feather="hash"></span>
                    </div>
                    <input type="number" name="numExterior" class="form-control" id="numExterior"
                        placeholder="No exterior">
                </div>
            </div>
            <div class="col-auto m-3">
                <label class="visually-hidden" for="calle">Calle</label>
                <div class="input-group">
                    <div class="input-group-text"> <span data-feather="map"></span>
                    </div>
                    <input type="text" name="calle" class="form-control" id="calle" placeholder="Calle">
                </div>
            </div>

            <div class="col-auto m-3">
                <label class="visually-hidden" for="email">Email</label>
                <div class="input-group">
                    <div class="input-group-text"> <span data-feather="mail"></span>
                    </div>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Email">
                </div>
            </div>
            <div class="col-auto m-3">
                <label class="visually-hidden" for="descripcion">Descripción</label>
                <div class="input-group">
                    <div class="input-group-text"> <span data-feather="type"></span>
                    </div>
                    <textarea class="form-control" name="descripcion" id="" rows="3"
                        placeholder="Descripción de la duda laboral"></textarea>
                </div>
            </div>

            <div class="col-auto m-3">
                <button type="submit" class="btn btn-primary">Enviar</button>

            </div>

        </div>
    </form>


@endsection
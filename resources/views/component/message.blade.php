@if(session()->has('msj'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session('msj') }}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif

@if(session()->has('msjError'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>{{ session('msjError') }}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif
<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
  <div class="position-sticky pt-4 scroll">
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="#">
          <i data-feather="home"></i>{{ Auth::user()->name }}  
        </a>
      </li>

      <!-- Split  button Asesorias-->
      <div class="btn-group m-1">
        <button type="button" class="btn btn-primary">Asesoría Labor</button>
        <button type="button" class="btn btn-warning " data-bs-toggle="dropdown">
          <span data-feather="chevrons-down"></span>
        </button>
        <ul class="dropdown-menu">
          <li class="nav-item">
            <li><h6 class="nav-link">Asesorías Laborales</h6></li>
            @role('super-admin|PDT|JI|SEECAT')                                    
            <li>
              <hr class="dropdown-divider"> 
            <a class="nav-link" href="{{ route('asesoria.index') }}">
              <span data-feather="inbox"></span>
              Recibidos
            </a>
          </li>
          @endrole
          @role('super-admin') 
          <li class="nav-item">
            <a class="nav-link" href="{{ route('asesoria.index',1) }}">
              <span data-feather="check-square"></span>
              Concluidos
            </a>
          </li>
          @endrole         
            <hr class="dropdown-divider">
          
        </ul>
      </div>

      <!-- Split  button Administrador-->
      @role('super-admin')        
      <div class="btn-group m-1">
        <button type="button" class="btn btn-primary"> Administrador</button>
        <button type="button" class="btn btn-warning " data-bs-toggle="dropdown">
          <span data-feather="chevrons-down"></span>
        </button>
        <ul class="dropdown-menu">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('user.index') }}">
              <span data-feather="users"></span>
              Usuarios
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('role.index') }}">
              <span data-feather="briefcase"></span>
              Roles
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('permiso.index') }}">
              <span data-feather="key"></span>
              Permisos
            </a>
          </li>
          <li>
            <hr class="dropdown-divider">
          </li>
        </ul>
      </div>
      @endcan
      

      

      @role('super-admin')
        <li class="nav-item">
          <a class="nav-link" href="#">
            <span data-feather="shopping-cart"></span>
            Inventario
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">
            <span data-feather="users"></span>
            Usuarios
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">
            <span data-feather="bar-chart-2"></span>
            Reportes
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">
            <span data-feather="dollar-sign"></span>
            Finanzas
          </a>
        </li>
    </ul>
  @else
    Usted no puede ver esta sección
    @endcan
    <ul class="nav flex-column mb-2">
      <li class="nav-item">

      </li>
    </ul>
    <!-- Right Side Of Navbar -->
    <ul class="nav flex-column mb-2 ">
      <!-- Authentication Links -->
      @guest
        <li class="nav-item">
          <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link"
            href="{{ route('register') }}">{{ __('Register') }}</a>
        </li>
      @else
        <li class="nav-item ">
          <a class="nav-link bg-dark text-light" href="{{ route('logout') }}" onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
            <span data-feather="log-out"></span>
            Salir  
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
      @endguest
    </ul>
  </div>
</nav>

<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h4">Dashboard</h1>
  </div>

  <h4>@yield('Subtitle')</h4>
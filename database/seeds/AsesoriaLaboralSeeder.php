<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Servicios\Asesoria AS AsesoriaLaboral;
use Illuminate\Support\Str;




class AsesoriaLaboralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        for ($i=0; $i < 20; $i++) { 
            # code...
            $asesoria = new AsesoriaLaboral();
            $asesoria->nombre=Str::random(10);
            $asesoria->telefono=354;
            $asesoria->instancia=2;
            $asesoria->email=Str::random(10).'@gmail.com';
            $asesoria->descripcion=Str::random(100);
            $asesoria->save();

        }
   



    }
}

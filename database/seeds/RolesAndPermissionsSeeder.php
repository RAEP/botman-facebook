<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class RolesAndPermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'editar']);
        Permission::create(['name' => 'eliminar']);
        Permission::create(['name' => 'publicar']);
        Permission::create(['name' => 'despublicar']);
        Permission::create(['name' => 'responder']);


        // create roles and assign created permissions

        // this can be done as separate statements
        $role = Role::create(['name' => 'PDT']);
        $role->givePermissionTo('responder');


        $role = Role::create(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());

        //Create super-admin user 

        $user = new User();
        $user->name="Administrador";
        $user->email="admin@admin.com";
        $user->password=bcrypt('secret');
        $user->assignRole('super-admin');       
        $user->save();
    }
}

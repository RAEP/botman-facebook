<?php
use App\Models\AsesoriaLaboral AS AsesoriaLaboral;
use Faker\Generator as Faker;

$factory->define(AsesoriaLaboral::class, function (Faker $faker) {
    return [
        //
        'nombre'=> $faker->firstName,
        'pApellido'=> $faker->lastName,
        'sApellido'=> $faker->lastName,
        'numExterior'=> $faker->buildingNumber,
        'calle'=> $faker->streetName,
        'email'=> $faker->freeEmail,
        'descripcion'=>$faker->text($maxNbChars=100),

    ];
});

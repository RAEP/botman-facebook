<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/chat', function () {
    return view('welcome');
});
Route::get('/', function () {
    return redirect()->route('asesoria.index');
});


Route::match(['get', 'post'], '/botman', 'BotManController@handle');
Route::get('/botman/tinker', 'BotManController@tinker');

Auth::routes();



Route::group(['middleware' => 'auth','prefix' => 'portal'], function() {
    //
    
    Route::group(['prefix' => 'servicios'], function() {
        //

        Route::group(['prefix' => 'asesorias'], function() {
            //
            Route::get('/{status?}','Servicios\AsesoriaController@index')->name('asesoria.index');
            Route::get('/solicitud_detalle/{id}','Servicios\AsesoriaController@show')->name('asesoria.show');
            Route::post('/cambio_instancia','Servicios\AsesoriaController@cambioInstancia')->name('asesoria.instancia');
            Route::post('/envio_respuesta','EmailController@contact')->name('asesoria.respuesta');     
        });
        
    });

    Route::group(['middleware' => 'role:super-admin','prefix' => 'admin'], function() {

    
        Route::group(['prefix' => 'usuarios'], function() {
            //
            Route::get('/','UserController@index')->name('user.index');
            Route::post('/','UserController@store')->name('user.store');
            Route::get('/new_user','UserController@create')->name('user.create');
            Route::get('/destroy_user/{id}','UserController@destroy')->name('user.destroy');

        });
        
            
        Route::group(['prefix' => 'roles'], function() {
            //
            Route::get('/','Admin\RoleController@index')->name('role.index');
            Route::get('/new_role','Admin\RoleController@create')->name('role.create');
            Route::post('/new_role','Admin\RoleController@store')->name('role.store'); 
            Route::get('/destroy_role/{id}','Admin\RoleController@destroy')->name('role.destroy');

          
        });
              
        Route::group(['prefix' => 'permisos'], function() {
            //
            Route::get('/','Admin\PermisoController@index')->name('permiso.index');
            Route::get('/new_permiso','Admin\PermisoController@create')->name('permiso.create');
            Route::post('/new_permiso','Admin\PermisoController@store')->name('permiso.store'); 
            Route::get('/destroy_permiso/{id}','Admin\PermisoController@destroy')->name('permiso.destroy'); 

        });
        
    });
    
         
});






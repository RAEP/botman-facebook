<?php

namespace App\Http\Controllers\Servicios;

use App\Models\Servicios\Asesoria AS Asesoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Crypt;


class AsesoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status=null)
    {
        //

         $solicitudes = null;
        if ($status==null){
            $solicitudes = Asesoria::where('atendido', 0)->get();
        }else{
            $solicitudes = Asesoria::where('atendido', 1)->get();
        }

            
        return view('servicios.asesoria.index',compact('solicitudes'));
   
    }

   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('servicios.asesoria.create');

        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         //Varibles de sesion,con mensajes de alerta
         
         DB::beginTransaction();
         try {
             //code...
             Asesoria::create($request->all());
         } catch (\Throwable $th) {
             //throw $th;
             DB::rollback();
             //return 'ERROR (' . $th->getLine() . '): ' . $th->getMessage() . ' CodeError ('.$th->getCode().')';
             return back()->with('msjError','No se registro la solicitud');            
        
         }
         
         DB::commit();
         
         
             
            return back()->with('msj','Solicitud enviada');
  
        

    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
  
    public function show($id)
    {
        //
        
        DB::beginTransaction();
        try {
            $id =  Crypt::decrypt($id);

            $solicitud = Asesoria::findOrFail($id);

        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            //return 'ERROR (' . $th->getLine() . '): ' . $th->getMessage() . ' CodeError ('.$th->getCode().')';
            return back()->with('msjError','No se encontro solicitud');            
       
        }
        
        DB::commit();
        

        return view('servicios.asesoria.show', compact('solicitud'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Asesoria $asesoriaLaboral)
    {
        //
        
    }


    public function cambioInstancia(Request $request)
    {
        
        DB::beginTransaction();
        try {
            
            $id= $request->id;
            $instancia = $request->instancia;
            $asesoria = Asesoria::findOrFail($id);
            $asesoria->instancia = $instancia;
            $asesoria->save();
        } catch (\Throwable $th) {
            //throw $th;

            DB::rollback();
            //return 'ERROR (' . $th->getLine() . '): ' . $th->getMessage() . ' CodeError ('.$th->getCode().')';
            return back()->with('msjError','No se logro registrar');            
        }
        
        DB::commit();
        
        

        return redirect()->route('asesoria.index')->with('msj','Solicitud enviada');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        DB::beginTransaction();
        try {
            //code...
            $id =  Crypt::decrypt($id);
            Asesoria::destroy($id);

        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            //return 'ERROR (' . $th->getLine() . '): ' . $th->getMessage() . ' CodeError ('.$th->getCode().')';
            return back()->with('msjError','No se logro registrar');            
       
        }

        
        DB::commit();
        
        return redirect()->route('asesoria.index')->with('msj','Solicitud Eliminada'); 
    }
}

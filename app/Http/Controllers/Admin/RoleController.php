<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Crypt;

use DB;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $roles = Role::all();

        return view('admin.roles.index',compact('roles'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $all_roles_in_database = Permission::all()->pluck('name');

        return view('admin.roles.create',compact('all_roles_in_database'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        DB::beginTransaction();

        try {
            $role = Role::create(['name' => $request->role]);
            $role->save();
            $role->syncPermissions($request->input('permisos', [])); // Si no hay ninguno pasa un array vacío


        }
        // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
        catch (\Exception $e)
        {
                DB::rollback();
                // no se... Informemos con un echo por ejemplo
                return back()->with('msjError','No se logro registrar');
                //echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
        }

        // Hacemos los cambios permanentes ya que no han habido errores
        DB::commit();
    
        return back()->with('msj','Registro guardado');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try {
            //code...
            $id =  Crypt::decrypt($id);

            Role::destroy($id);

        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            //return 'ERROR (' . $th->getLine() . '): ' . $th->getMessage() . ' CodeError ('.$th->getCode().')';
            return back()->with('msjError','No se logro eliminar');            
       
        }

        
        DB::commit();
        
        return back()->with('msj','Role eliminado'); 
    }
}

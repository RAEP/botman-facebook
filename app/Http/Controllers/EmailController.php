<?php

namespace App\Http\Controllers;

use App\Models\Servicios\Asesoria;

use Illuminate\Http\Request;
use Mail; //Importante incluir la clase Mail, que será la encargada del envío
use DB;

class EmailController extends Controller
{
    public function contact(Request $request){

        DB::beginTransaction();

        try {
            $id = $request->id;
            $asesoria = Asesoria::findOrFail($id);
            $data = $request->all();

            foreach ($data as $key => $value) {
                # code...
                $respuesta = $value;
            }

            $for = $asesoria->email;            
            $subject = "Respuesta de solicitud a styps";
            $from = "stypsinformatica@gmail.com";
            
            Mail::send('mails/servicios/asesoria/respuesta',compact('respuesta','asesoria'), function($msj) use($subject,$for,$from){
                $msj->from($from,"STYPS");
                $msj->subject($subject);
                $msj->to($for);
            });

            $asesoria->atendido = 1;
            $asesoria->respuesta = $respuesta;
            $asesoria->save();
            }
        // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
        catch (\Throwable $th)
        {
                DB::rollback();
                // no se... Informemos con un echo por ejemplo
                //return back()->with('msjError','No se logro enviar la respuesta');
            return 'ERROR (' . $th->getLine() . '): ' . $th->getMessage() . ' CodeError ('.$th->getCode().')';
        }

        // Hacemos los cambios permanentes ya que no han habido errores
        DB::commit();
       

        return redirect()->route('asesoria.index')->with('msj','Respuesta enviada');
    }
}

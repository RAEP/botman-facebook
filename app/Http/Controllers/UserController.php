<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        $users = DB::table('model_has_roles')
        ->join('users', 'model_has_roles.model_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
        ->select('roles.name as roleName' ,'users.name as userName','users.id','users.email','users.created_at')
        ->get();
        
        return view('admin.users.index', compact('users'));
        


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $all_roles_in_database = Role::all()->pluck('name');

        
        return view('auth/register', compact('all_roles_in_database'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         //

         DB::beginTransaction();

         try {
            
            $user =User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);
            $user->assignRole($request->role);
 
         }
         // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
         catch (\Exception $e)
         {
                 DB::rollback();
                 // no se... Informemos con un echo por ejemplo
                 //return back()->with('msjError','No se logro registrar');
                 echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
         }
 
         // Hacemos los cambios permanentes ya que no han habido errores
         DB::commit();
     
         return back()->with('msj','Registro guardado');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try {
            //code...
            $id =  Crypt::decrypt($id);

            User::destroy($id);

        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            //return 'ERROR (' . $th->getLine() . '): ' . $th->getMessage() . ' CodeError ('.$th->getCode().')';
            return back()->with('msjError','No se logro eliminar');            
       
        }

        
        DB::commit();
        
        return back()->with('msj','Usuario eliminado'); 
    }
}

<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use App\Models\AsesoriaLaboral AS AsesoriaLaboral;


class StypsConversation extends Conversation
{

    /**
     * First question
     */
    public function askPrimeraInteraccion()
    {
        $question = Question::create("¿Cómo podemos ayudarte?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                Button::create('Dudas')->value('duda'),
                Button::create('Solicitud de empleo')->value('empleo'),
                Button::create('Programas')->value('programas'),

            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'duda') {
                    //Conversación de dudas
                    $this->askDudas();
                } elseif ($answer->getValue() === 'empleo') {
                    # code...
                    $this->say('No manejo la información de empleos que necesitas,
                    pero se quien tiene esa información consulta las vacantes en SECAT.
                    https://qroo.gob.mx/styps/seecat');
                    $this->askFinalizador();
                
                }else{
                    //Conversación de programas
                }

            }
        });
    }

    // Soluciones de dudas
    public function askDudas()
    {
        $question = Question::create("¿Qué necesitas conocer?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                Button::create('Funciones del STYPS')->value('funcion'),
                Button::create('Que es el aguinaldo')->value('aguinaldo'),
                Button::create('Qué es empleo por contrato')->value('contrato'),

            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'dudas') {

                    $this->say('Somos una institución de gobierno');
                    $this->askFinalizador();

                  

                } elseif ($answer->getValue() === 'empleo') {
                    # code...
                    $this->say('No manejo la información de empleos que necesitas,
                    pero se quien tiene esa información consulta las vacantes en SECAT.
                    https://qroo.gob.mx/styps/seecat');
                    $this->askFinalizador();
                
                }else{
                    $this->say('Son personas con contrato'); 
                    $this->askFinalizador();

                }
            }
        });
    }

   // Finalizador de respuestas concretas
   public function askFinalizador()
   {
       $question = Question::create("¿Te fui de ayuda?")
           ->fallback('Unable to ask question')
           ->callbackId('ask_reason')
           ->addButtons([
               Button::create('Si')->value('si'),
               Button::create('No,tengo otro problema')->value('no'),
               Button::create('Empezar de nuevo')->value('empezar'),

           ]);

       return $this->ask($question, function (Answer $answer) {
           if ($answer->isInteractiveMessageReply()) {
               if ($answer->getValue() === 'si') {
                 
                   $this->SolucionBot();

               } elseif ($answer->getValue() === 'no') {
                            
                   $this->SolucionAgente();
               }else{
                $this->askPrimeraInteraccion();

               }
           }
       });
   }
    //Finalizadores


    public function askDescripcion()
    {
        $this->ask('Por favor escribe cual es tu duda y nos pondremos en contacto contigo', function(Answer $answer) {
            // Save result
            $this->descripcion = $answer->getText();            
            $this->askConfirmarSolicitud();
        });
       
    }

    public function askConfirmarSolicitud()
   {
       $question = Question::create("¿Todo bien?")
           ->fallback('Unable to ask question')
           ->callbackId('ask_reason')
           ->addButtons([
               Button::create('Todo correcto')->value('si'),
               Button::create('Ups, me equivoqué')->value('no'),
               Button::create('Empezar de nuevo')->value('empezar'),

           ]);

       return $this->ask($question, function (Answer $answer) {
           if ($answer->isInteractiveMessageReply()) {
               if ($answer->getValue() === 'si') {
                 
                   $this->SolucionAgente();

               } elseif ($answer->getValue() === 'no') {
                   # code...
                               
                   $this->askDescripcion();
               }else{
                    $this->askPrimeraInteraccion();
               }

           }
       });
   }
   public function SolucionBot()
   {
       $this->ask('Genial me encanto ayudarte, nos vemos pronto', function(Answer $answer) {
           // Save result
           $solicitud = AsesoriaLaboral::create([
               'nombre' => $this->nombre,
               'telefono' => $this->telefono,
               'email' => $this->email,
               'atendido' => $this->1,
           ]);
           
           $this->say('Registrado ');
       });

       public function SolucionAgente()
       {
           $this->ask('Contestaremos tu duda lo más pronto posible, gracias por usar nuestros servicios.', function(Answer $answer) {
               // Save result
               $solicitud = AsesoriaLaboral::create([
                   'nombre' => $this->nombre,
                   'telefono' => $this->telefono,
                   'email' => $this->email,
                   'descripcion' => $this->descripcion,
               ]);               
           });
       
      
   }
    /**
     * Start the conversation
     */
    public function run()
    {
        $this->Guardar();
    }
}

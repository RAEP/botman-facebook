<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use App\Models\Servicios\Asesoria AS AsesoriaLaboral;


class StypsConversation extends Conversation
{

    protected $nombre;
    protected $telefono;
    protected $email;
    protected $descripcion;
    




    /**
     * First question
     */
    public function askNombre()
    {
        $this->ask('Hola! ¿Cual es tu nombre completo?', function(Answer $answer) {
            // Save result
            $this->nombre = $answer->getText();

            $this->say('Mucho gusto '.$this->nombre);
            $this->askTelefono();
        });
       
    }
    public function askTelefono()
    {
        $this->ask('Para un mejor servicio necesitamos tu número de teléfono', function(Answer $answer) {
            // Save result
            $this->telefono = $answer->getText();
            $this->say('¡Excelente!');
            $this->askEmail();

        });
       
    }

    public function askEmail()
    {
        $this->ask('Una cosa más, ¿Cúal es tu correo electrónico?', function(Answer $answer) {
            // Save result
            $this->email = $answer->getText();
    
            $this->say('Gracias por la confianza '.$this->nombre);
            $this->askConfirmaPrimerosDatos();
        });
       
    }

    public function askConfirmaPrimerosDatos()
    {
        $question = Question::create("¿Todos los datos son correctos?")
            ->fallback('Unable to ask question')
            ->callbackId('askConfirmaPrimerosDatos')
            ->addButtons([
                Button::create('Todo correcto')->value('si'),
                Button::create('Ups, me equivoqué')->value('no'),
            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'si') {
                    $this->askPrimeraInteraccion();

                } else {
                    $this-> askNombre();
                }
            }
        });
    }
   

   
    public function askPrimeraInteraccion()
    {
        $question = Question::create("¿Cómo podemos ayudarte?")
            ->fallback('Unable to ask question')
            ->callbackId('askPrimeraInteraccion')
            ->addButtons([
                Button::create('Dudas')->value('duda'),
                Button::create('Solicitud de empleo')->value('empleo'),
                Button::create('Programas')->value('programas'),

            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'duda') {
                    //Conversación de dudas
                    $this->askDudas();
                } 
                if ($answer->getValue() === 'empleo') {
                    # code...
                    $this->say('No manejo la información de empleos que necesitas,
                    pero se quien tiene esa información consulta las vacantes en SECAT.
                    https://qroo.gob.mx/styps/seecat');
                    $this->askFinalizador();
                
                }if ($answer->getValue() === 'programas'){
                    //Conversación de programas
                    $this->say('programas');
                    $this->askFinalizador();
                }

            }
        });
    }

    // Soluciones de dudas
    public function askDudas()
    {
        $question = Question::create("¿Qué necesitas conocer?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                Button::create('Funciones del STYPS')->value('funcion'),
                Button::create('Que es el aguinaldo')->value('aguinaldo'),
                Button::create('Qué es empleo por contrato')->value('contrato'),

            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'funcion') {

                    $this->say('Somos una institución de gobierno');
                    $this->askFinalizador();

                } if ($answer->getValue() === 'aguinaldo') {
                    # code...
                    $this->say('No manejo la información de empleos que necesitas,
                    pero se quien tiene esa información consulta las vacantes en SECAT.
                    https://qroo.gob.mx/styps/seecat');
                    $this->askFinalizador();
                
                }if ($answer->getValue() === 'contrato'){
                    $this->say('Son personas con contrato'); 
                    $this->askFinalizador();

                }
            }
        });
    }

     // Finalizador de respuestas concretas
   public function askFinalizador()
   {
       $question = Question::create("¿Te fui de ayuda?")
           ->fallback('Unable to ask question')
           ->callbackId('askFinalizador')
           ->addButtons([
               Button::create('Si')->value('si'),
               Button::create('No,tengo otro problema')->value('no'),
               Button::create('Empezar de nuevo')->value('empezar'),

           ]);

       return $this->ask($question, function (Answer $answer) {
           if ($answer->isInteractiveMessageReply()) {
               if ($answer->getValue() === 'si') {

                $flight = AsesoriaLaboral::create([
                    'nombre' => $this->nombre,
                    'telefono' => $this->telefono,
                    'email' => $this->email,
                    'atendido' => 1,
                ]);                                            
                
                $this->Guaradar();
               } 
               if ($answer->getValue() === 'no') {
                            
                   $this->askDescripcion();
               }
               if ($answer->getValue() === 'empezar'){
                $this->askPrimeraInteraccion();

               }
           }
       });
   }

   public function Guaradar()
   {
       $this->ask('Genial me encanto ayudarte, nos vemos pronto', function(Answer $answer) {
           // Save result
       });             
   }

   
    public function askDescripcion()
    {
        $this->ask('Por favor escribe cual es tu duda y nos pondremos en contacto contigo', function(Answer $answer) {
            // Save result
            $this->descripcion = $answer->getText();
    
            $this->say('Gracias por la confianza '.$this->descripcion);
            $this->askConfirmarSolicitud();
        });
       
    }

    public function askConfirmarSolicitud()
    {
        $question = Question::create("¿Todo bien?")
            ->fallback('Unable to ask question')
            ->callbackId('askConfirmarSolicitud')
            ->addButtons([
                Button::create('Todo correcto')->value('si'),
                Button::create('Ups, me equivoqué')->value('no'),
                Button::create('Empezar de nuevo')->value('empezar'),
 
            ]);
 
        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'si') {

                    $flight = AsesoriaLaboral::create([
                        'nombre' => $this->nombre,
                        'telefono' => $this->telefono,
                        'email' => $this->email,
                        'descripcion' => $this->descripcion,
                        'atendido' => 0,
                    ]);
                  
                    $this->SolucionAgente();
 
                } 
                if ($answer->getValue() === 'no') {
                    # code...
                                
                    $this->askDescripcion();
                }
                if ($answer->getValue() === 'empezar'){
                     $this->askPrimeraInteraccion();
                }
 
            }
        });
    }
    
    public function SolucionAgente()
    {
        $this->ask('Contestaremos tu duda lo más pronto posible, gracias por usar nuestros servicios.', function(Answer $answer) {

        });
        
       
    }
    /**
     * Start the conversation
     */
    public function run()
    {
        $this->askNombre();
    }
}

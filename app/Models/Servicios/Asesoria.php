<?php

namespace App\Models\Servicios;

use Illuminate\Database\Eloquent\Model;

class Asesoria extends Model
{
     //

    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','telefono', 'email','atendido','descripcion',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
